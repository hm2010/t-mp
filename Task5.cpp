#include <iostream>
#include <string>
#include <vector>
using namespace std;

vector<int> tree;
vector<int> output;

int main(){ 
        int n; 
        string command;
        int element;
        int size;
        cin >> n;
        for (int i = 0; i < n; i++){
                size = tree.size();
                cin >> command;
                if (command == "add"){
                        cin >> element;
                        if (size == 0){
                                tree.push_back(element);
                        }
                        else{

                                bool isFound = false;  
                                for (int j = 0; j < size; j++){
                                        if (element < tree[j]){
                                                tree.insert(tree.begin() + j, element);
                                                isFound = true;
                                                break;
                                        }
                                }
                                if (!isFound){
                                        tree.push_back(element);
                                }
                        }
                }
                else if (command == "get"){
                        output.push_back(tree.front());
                }
                else{
                        tree.erase(tree.begin());
                }
        }
        while (output.size() != 0){
                cout << output.front() << endl;
                output.erase(output.begin());
        }
        return 0;
}